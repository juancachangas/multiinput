import React, { Component } from 'react'
import PropTypes from 'prop-types';
export class MultiInput extends Component {
  constructor(props) {
    super(props)
    this.state = {
      inputs: this.props.value? [...this.props.value, ''] : ['']
    }
  }
  onChange = (e, index) => {
    const value = e.target.value
    this.setState(prevState => {
      let {inputs} = prevState
      if (((index + 1) === prevState.inputs.length) && 
        inputs[index] === ''    
      ){
        inputs.push('')
      }

      inputs[index] = value
      return {
        inputs
      }
    })
  }
  remove = (index) => {
    this.setState(prevState => {
      let inputs = prevState.inputs.filter((_, i) => index !== i)
      return {
        inputs
      }
    })
  }
  clean = _ => {
    this.setState({inputs: ['']})
  }
  render(){
    const {inputs} = this.state
    return (
      <div className='row'>
        <div className='col s6'>
          <div className='card'>
            <section className='card-content'>

            <p className='card-title'>Test</p>
            {inputs.map((input, index) => {
              return (<article className='input' key={index}>
                <div class="input-field col s11">
                  <label>Test Attribute</label>
                  <input type='text' value={input} onChange={e => this.onChange(e,index)} key={index} />
                </div>
                {(index + 1) !== inputs.length && <button class='btn-large btn-flat col s1' onClick={_ => this.remove(index)}>x</button>}
              </article>)
            })}
            </section>
            <div class="card-action blue lighten-5">
              <a href="javascript:void(0)" className="grey-text" onClick={this.clean}>Cancel</a>
              <a href="javascript:void(0)" className="blue-text darken-3" onClick={_ => 
                this.props.onChange(inputs.slice(0,this.state.inputs.length-1))
                }>Save</a>
            </div>
          </div>
        </div>
      </div>
      )
  }
}
MultiInput.propTypes = {
  value: PropTypes.arrayOf(PropTypes.string),
  onChange: PropTypes.func.isRequired
} 
