import React, { Component } from 'react'
import './App.css'
import { MultiInput } from './MultiInput'

class App extends Component {
  render() {
    return (
      <MultiInput value={['first','second']} onChange={inputs => console.log(inputs)} />
    );
  }
}

export default App;
